import os
import sys
import re
import glob
import stat
import subprocess
import binascii
import multiprocessing
from datetime import datetime
import shutil
import hashlib
import ast
import time
import argparse
'''
SPIE Intraperiod Settings

SPIE2020 8bit : 128
SPIE2020 10bit : 119
SPIE2021 : -1
'''

'''Add special parameters to commands for testing (i.e. --pred-struct 1, --tune 0)'''


TEST_SETTINGS = {
    ##'''Test Parameters'''##
    'test_name': [''],
    'stream_dir': r'/home/ubuntu/stream/',
    'presets': [0,1,2,3,4],
    'intraperiod': -1,
    'decode_cycles': 0,

    ##'''System Parameters'''##
    'decode_threads': 3,
    'decode_jobs': 20,
    'num_pool': multiprocessing.cpu_count(),
    'ffmpeg_job_count': multiprocessing.cpu_count() // 2,#20,
    'vmaf_job_count': 96,
    'insert_special_parameters' : [],
}

EXECUTION_SETTINGS = {
    'run_after_generation' : 0,
    'commit' : '',
    }


def test_configurations():
    TEST_REPOSITORY = {
        'SPIE2021_svt'       : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_svt_CRF_1lp_1p']],
        'SPIE2021_aom'       : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],
        'SPIE2021_x264'      : [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_x264_CRF_1p']],
        'SPIE2021_x265'      : [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_x265_CRF_1p']],
        'SPIE2021_vp9'       : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_vp9_CRF_2p']],
        'SPIE2021_vvenc'     : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale_vvenc'], ENCODE_COMMAND['SPIE2021_vvenc_CRF_1p']],
        'SPIE2021_svt_cqp'   : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['svt_CRF_1lp_1p_aq0']],
        'SPIE2021_avm_5L'    : [RC_VALUES['avm_11qp'],           RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_avm_ffmpeg_rescale'],           ENCODE_COMMAND['avm_CRF_1p']],
        'SPIE2021_vtm_RA'    : [RC_VALUES['SPIE2021_svt_aom'],   RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_vtm_rescale_ffmpeg'],   ENCODE_COMMAND['vtm_CRF_1p']],


        'SPIE2021_ffmpeg_svt_fast_decode': [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['ffmpeg_svt_fast_decode']],

        'SPIE2020_8bit_svt'  : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'SPIE2020_8bit_aom'  : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_aom_CRF_2p']],
        'SPIE2020_8bit_x264' : [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_8bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x264_CRF_1p']],
        'SPIE2020_8bit_x265' : [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_8bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x265_CRF_1p']],
        'SPIE2020_8bit_vp9'  : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_vp9_CRF_2p']],


        'SPIE2020_10bit_svt' : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'SPIE2020_10bit_aom' : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_aom_CRF_2p']],
        'SPIE2020_10bit_x264': [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_10bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x264_CRF_1p']],
        'SPIE2020_10bit_x265': [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_10bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x265_CRF_1p']],
        'SPIE2020_10bit_vp9' : [RC_VALUES['SPIE2020_svt_aom'],   RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_vp9_CRF_2p']],


        #'xilften_svt': [RC_VALUES['xilften_crf'], RESOLUTION['xilften'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['xilften_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'xilften_svt': [RC_VALUES['xilften_crf'], RESOLUTION['xilften_test'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['xilften_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'xilften_libaom': [RC_VALUES['xilften_crf'], RESOLUTION['xilften'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['xilften_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],

        'SPIE2021_svt_for_testing': [RC_VALUES['fast_testing_qps'], RESOLUTION['fast_testing_resolutions'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_svt_CRF_1lp_1p']],

        'SPIE2020_ffmpeg_svt': [RC_VALUES['ffmpeg_svt'], RESOLUTION['ffmpeg_svt'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale_psnr_ssim'], ENCODE_COMMAND['ffmpeg_svt_embedded_scaling']],

        #MR testing
        'svt_CRF_1lp_1p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'svt_CRF_1lp_2p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_CRF_1lp_2p']],
        'svt_CRF_nonlp_1p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_CRF_nonlp_1p']],
        'svt_CRF_nonlp_2p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_CRF_nonlp_2p']],
        'svt_VBR_1lp_1p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_VBR_1lp_1p']],
        'svt_VBR_1lp_2p_MR': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['svt_VBR_1lp_2p']],

        #webrtc
        'webrtc_svt': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['svt_webrtc']],
        'webrtc_svt_SC': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['svt_webrtc_SC']],
        'webrtc_svt_ultra_LD': [RC_VALUES['webrtc_ultra_LD'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['svt_webrtc']],
        'webrtc_aom': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['aom_webrtc']],
        'webrtc_aom_SC': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['aom_webrtc_SC']],
        'webrtc_svt_iterations-3': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['svt_webrtc']],
        'webrtc_svt_SC_iterations-3': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['svt_webrtc_SC']],
        'webrtc_aom_iterations-3': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['aom_webrtc']],
        'webrtc_aom_SC_iterations-3': [RC_VALUES['webrtc'], None, None, METRIC_COMMAND['webrtc_psnr_ssim'], ENCODE_COMMAND['aom_webrtc_SC']],

        #Performance Tracking
        'aom_test' : [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],
        'svt_test'  : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_svt_CRF_1lp_1p']],


        #preset tuning
        'svt_CRF_1lp_1p_tuning_5qp': [RC_VALUES['5qp_preset_tuning'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim_vmaf_neg'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'svt_CRF_1lp_1p_tuning_11qp': [RC_VALUES['11qp_preset_tuning'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim_vmaf_neg'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        
        'svt_CRF_lp8_1p_tuning_5qp': [RC_VALUES['5qp_preset_tuning'], None, None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim_vmaf_neg'], ENCODE_COMMAND['svt_CRF_lp8_1p']],

        #5QP vtm/vvenc/svt 480p testings
        'vtm_5qp_RA_test' : [RC_VALUES['vvenc_5qp'], None,   None, METRIC_COMMAND['SPIE2021_vtm_ffmpeg'], ENCODE_COMMAND['vtm_CRF_1p']],

        'vvenc_5qp_crf_closedGOP' : [RC_VALUES['vvenc_5qp'], None,   None, METRIC_COMMAND['SPIE2021_vvenc_ffmpeg'], ENCODE_COMMAND['SPIE2021_vvenc_CRF_1p_closedGOP']],
        'vvenc_5qp_crf_openGOP' : [RC_VALUES['vvenc_5qp'], None,   None, METRIC_COMMAND['SPIE2020_ffmpeg_psnr_ssim'], ENCODE_COMMAND['SPIE2021_vvenc_CRF_1p']],        

        'svt_CRF_1lp_1p_open_gop': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2021_ffmpeg_vmaf'], ENCODE_COMMAND['svt_CRF_1lp_1p_open_gop']],
        'svt_CRF_1lp_1p_closed_gop': [RC_VALUES['svt_mr_testing'], None, None, METRIC_COMMAND['SPIE2021_ffmpeg_vmaf'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        
        'avm_test_5L' : [RC_VALUES['avm'], None, None, METRIC_COMMAND['SPIE2021_avm_ffmpeg'], ENCODE_COMMAND['avm_CRF_1p']],
        'avm_test_6L' : [RC_VALUES['avm'], None, None, METRIC_COMMAND['SPIE2021_avm_ffmpeg'], ENCODE_COMMAND['avm_CRF_6L_1p']],

        #360p Elfuente
        'SPIE2021_avm_5L_sub_360p' : [RC_VALUES['avm_11qp'],         RESOLUTION['SPIE2021_sub_360p'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_avm_ffmpeg'],           ENCODE_COMMAND['avm_CRF_1p']],
        'SPIE2021_vtm_RA_sub_360p'              : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_sub_360p'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_vtm_rescale_ffmpeg'],   ENCODE_COMMAND['vtm_CRF_1p']],
        'SPIE2021_svt_cqp_sub_360p'              : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_sub_360p'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['svt_CRF_1lp_1p_aq0']],
        'SPIE2021_aom_sub_360p'                  : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_sub_360p'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'],       ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],
        'SPIE2021_vvenc_sub_360p'                : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_sub_360p'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale_vvenc'], ENCODE_COMMAND['SPIE2021_vvenc_CRF_1p']],

    }

    return TEST_REPOSITORY



def main():
    '''Process Command Line Arguments'''
    parse_command_line()
        
    for test_name in TEST_SETTINGS['test_name']:
        '''Change to aom encoder if specified'''
        if "aom" in test_name.lower():
            EXECUTION_SETTINGS['git_url'] = 'https://aomedia.googlesource.com/aom'
        elif "svt" in test_name.lower():
            EXECUTION_SETTINGS['git_url'] = "https://gitlab.com/AOMediaCodec/SVT-AV1.git"
        
        if EXECUTION_SETTINGS['run_after_generation'] == 1:        
            test_dir = os.path.join(os.getcwd(), '{}_{}'.format(test_name, EXECUTION_SETTINGS['commit']))
            '''Build encoder'''
            #print('\n\n\n\nhere\n\n\n\n')
            get_encoder_binaries(test_name, test_dir)               
        else:
            test_dir = os.getcwd()
        print('test_dir',test_dir)
        '''Settings Check'''
        validate_test_configuration(test_name)
        
        '''Generate commands for test being performed'''
        generated_commands = process_command_template(test_name, test_dir)

        '''Write commands to file'''
        encode_file_id, metric_file_id, metric_id = write_commands_to_files(generated_commands, test_name, test_dir)

        '''Create the bash files to execute the commands'''
        bash_exe = generate_bash_driver_file(encode_file_id, metric_file_id, metric_id,test_name, test_dir)
        print('bash_exe',bash_exe)
        if EXECUTION_SETTINGS['run_after_generation']:
         
            print('Executing tests')
            execute_test(bash_exe,test_dir)
    
        
def validate_test_configuration(test_name):
    if not os.path.isdir(TEST_SETTINGS['stream_dir']):
        print(TEST_SETTINGS['stream_dir'])
        print('[ERROR] The stream folder specified does not exist. Exiting...')
        sys.exit()
    if test_name not in test_configurations():
        print('[ERROR] Invalid test configuration specified. Exiting...')
        sys.exit()
    if not test_name:
        print('No test Config Specified. Exiting...')
        sys.exit()
    if 'non' in test_name:
        TEST_SETTINGS['num_pool'] /= 5


def parse_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--test-name', help='Name of test to run')
    parser.add_argument('-s', '--stream', help='Target stream folder')
    parser.add_argument('-p', '--presets', help='presets to test')
    parser.add_argument('-i', '--intraperiod', help='intraperiod to test')
    parser.add_argument('-c', '--commit', help='Commit to tests')
    parser.add_argument('-a', '--added_params', help='Additional parameters to add')
    parser.add_argument('-r', '--run', help='Execute Tests After Generation')

    
    args = parser.parse_args()
    if args.test_name:
        TEST_SETTINGS['test_name'] = [args.test_name]
    if args.stream:
        TEST_SETTINGS['stream_dir'] = args.stream
    if args.presets:
        if ',' in args.presets:
            TEST_SETTINGS['presets'] = [int(x) for x in args.presets.split(',')]
        else:
             TEST_SETTINGS['presets'] = [int(args.presets.strip())]        
    if args.intraperiod:
        TEST_SETTINGS['intraperiod'] = args.intraperiod
    if args.commit:
        EXECUTION_SETTINGS['commit'] = args.commit
    if args.added_params:
        TEST_SETTINGS['insert_special_parameters'] = [args.added_params]
    if args.run:        
        EXECUTION_SETTINGS['run_after_generation'] = args.run


def get_encoder_binaries(test_name, test_dir):
    commit = EXECUTION_SETTINGS['commit']
    git_url = EXECUTION_SETTINGS['git_url']
    git_dir = os.path.join(test_dir,commit)

    '''Commit is MR if number''' 
    if commit.isdigit() and len(commit) == 4:
        ref_commit, mod_commit, source_branch = find_ref_mod_hashes(git_url)
        print('mod_commit',mod_commit)
        print('ref_commit',ref_commit)
        print('source_branch',source_branch)
        clone_commands = ["git clone {} {}".format(git_url, git_dir),
                    "cd {} && git fetch origin merge-requests/{}/head:{}".format(git_dir, commit, source_branch),
                    "cd {} && git checkout {}".format(git_dir, source_branch)]
    else:
        clone_commands = [
                    "git clone {} {}".format(git_url, git_dir),
                    "cd {} && git checkout {}".format(git_dir,commit)]

    if "aom" in test_name.lower():
        aom_build = os.path.join(test_dir,'aom_build')
        build_commands = ["mkdir -p {} ".format(aom_build),
                          "cd {} && cmake {} && make -j".format(aom_build, git_dir),
                          'cp {}/aomenc {}'.format(aom_build,test_dir)]
    else:
        build_commands = ["{}/Build/linux/build.sh static".format(git_dir),
                        "cp -a {}/Bin/Release/. {}".format(git_dir,test_dir)]

    for clone_command in clone_commands:
        execute_command(clone_command, os.getcwd())

    for build_command in build_commands:
        print('build_command',build_command)
        execute_command(build_command, os.getcwd())


def find_ref_mod_hashes(git_url):
    mr_number = EXECUTION_SETTINGS['commit']
    
    mr_url = '{}/api/v4/projects{}%2F{}/merge_requests/{}'.format('/'.join(git_url.split('/')[:3]) ,'/'.join(git_url.split('.com')[-1].split('/')[:-1]), git_url.split('/')[4].replace('.git',''), mr_number)
    print('curl -x http://proxy.sc.intel.com:911 -Ls -k  {}'.format(mr_url))
    merge_pipe = subprocess.Popen('curl -x http://proxy.sc.intel.com:911 -Ls -k  {}'.format(mr_url), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
    merge_commit_list = merge_pipe.communicate()[0]
##    print(merge_commit_list)
    mod_commit = re.findall(r'"sha":"(.*?)"',str(merge_commit_list))[0]
    ref_commit = re.findall(r'{"base_sha":"(.*?)"', str(merge_commit_list))[0]
    source_branch_name = re.findall(r'"source_branch":"(.*?)"', str(merge_commit_list))[0]
    
    return ref_commit, mod_commit, source_branch_name


'''   Core Sample Command processing functions  '''
def process_command_template(test_name, test_dir):
    generated_commands = dict()
    encode_commands = list()
    metric_commands = list()
    decode_commands = list()
    copy_commands = downscale_commands = ''
    
    '''Pull test info from Test repo'''

    rc_values = test_configurations()[test_name][0]
    resolutions = test_configurations()[test_name][1]
    downscale_command_template = test_configurations()[test_name][2]
    metric_command_template = test_configurations()[test_name][3]
    encoding_command_template = test_configurations()[test_name][4]


    '''Insert a few extra tokens for the case where we don't want to make a while new test configuration'''
    if TEST_SETTINGS['insert_special_parameters']:
        encoding_command_template = insert_new_parameters(encoding_command_template)

    for preset in TEST_SETTINGS['presets']:
        vvenc_presets = ["slower", "slow", "medium", "fast", "faster"]

        per_preset_encode_commands = list()
        per_preset_metric_commands = list()
        per_preset_decode_commands = list()

        clip_lists = sort_clip_list_by_complexity(TEST_SETTINGS['stream_dir'])

        if resolutions:
            copy_commands, downscale_commands, clip_lists, parameter_tracker = get_downscaled_clip_list(clip_lists, resolutions, downscale_command_template)

        '''Process iterations if any'''
        is_iteration = re.search(r"iterations-(\d+)", test_name)
        if is_iteration:
            iterations = int(is_iteration.group(1))
        else:
            iterations = 1

        for i in range(len(clip_lists)):
            for rc_value in rc_values:
                for clip in clip_lists[i]:
                    for iteration in range(iterations):
                        '''Retrieve the relevant parameters for the given clip'''
                        if clip.endswith('yuv') and resolutions:
                            width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = parameter_tracker[clip]
                        elif clip.endswith('y4m') and resolutions:
                            width, height, framerate, number_of_frames,target_width = parameter_tracker[clip]
                        elif clip.endswith('yuv') and yuv_library_found:
                            width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
                        elif clip.endswith('y4m'):
                            width, height, framerate, number_of_frames = read_y4m_header(clip)
                        else:
                            print('[Warning]: Skipping Clip: {}'.format(clip))
                            continue
##                        if 1080 == height:
##                            print('skipping')
##                            continue
                        '''Modify rc_value based on clip definition'''
                        if height >= 720 and 'webrtc' in test_name:
                            rc_value *= 4
                        
                        '''Set the intraperiod to +1 number of frames for encoders that do not support -1 keyint'''
                        if 'SvtAv1EncApp' not in encoding_command_template and str(TEST_SETTINGS['intraperiod']) == '-1':
                            intraperiod = number_of_frames + 1
                        else:
                            intraperiod = TEST_SETTINGS['intraperiod']

                        '''Set the pixfmt according to the bitdepth'''
                        if clip.endswith('yuv'):
                            if bitdepth == 10:
                                pixfmt = "yuv420p10le"
                                vvenc_pixfmt = 'yuv420_10'
                            else:
                                pixfmt = "yuv420p"
                                vvenc_pixfmt = 'yuv420'

                        '''Assign the rc identifier based on the number of digits'''
                        if len(str(rc_value)) > 2 or "webrtc" in test_name:
                            rc_string = 'TBR{}kbps'.format(rc_value)
                        else:
                            rc_string = 'Q{}'.format(rc_value)

                        '''Set the vvenc presets according to the preset number'''
                        if preset < 4:
                            vvenc_preset = vvenc_presets[preset]
                        else:
                            vvenc_preset = "faster"
                            
                        if resolutions:
                            '''Get reference clip resolution for the rescale case'''
                            ref_res = re.search(r'(\d+x\d+)to(\d+x\d+)', clip)

                            if ref_res:
                                ref_width = ref_res.group(1).split('x')[0]
                                ref_height = ref_res.group(1).split('x')[1]
                                mod_width = ref_res.group(2).split('x')[0]
                                mod_height = ref_res.group(2).split('x')[1]
                                ref_clip = re.sub('to\\d+x\\d+', '', clip)
                            else:
                                ref_width = mod_width = width
                                ref_height = mod_height = height
                                ref_clip = clip

                        '''Generic setup of parameters and conditionals'''
                        clip_name = os.path.split(clip)[1]
                        rawvideo = 'rawvideo'
                        vmaf_pixfmt = '420'

                        output_filename = '{}/{}_M{}_{}_{}'.format('bitstreams', test_name, preset, clip_name[:-4], rc_string)

                        '''Append iteration index to file name if the current iteration is > 0'''
                        if iteration > 0:
                            output_filename = "{}-{}".format(output_filename, int(iteration))
                        else:
                            output_filename = '{}/{}_M{}_{}_{}'.format('bitstreams', test_name, preset, clip_name[:-4], rc_string)

                        '''remove yuv tokens in case of y4m clip'''
                        if clip.endswith('y4m'):
                            encode_command = remove_yuv_tokens(encoding_command_template)
                            metric_command = remove_yuv_tokens(metric_command_template)
                        elif clip.endswith('yuv'):
                            encode_command = encoding_command_template
                            metric_command = metric_command_template

                        '''sub in the values'''
                        temp_clip = '/dev/shm/{}.{}'.format(output_filename, clip[-3:])
                        '''Generate Encode Commands'''
                        encode_command = encode_command.format(**vars())
                        '''Generate Metric Command'''
                        metric_command = metric_command.format(**vars())

                        if TEST_SETTINGS['decode_cycles']:
                            decode_command = generate_decode_commands(output_filename)
                            per_preset_decode_commands.append(decode_command)

                        per_preset_encode_commands.append(encode_command)
                        per_preset_metric_commands.append(metric_command)

                        '''Return rc_value back to original value for next clip'''
                        if height >= 720 and 'webrtc' in test_name:
                            rc_value //= 4
                        
        encode_commands.append(per_preset_encode_commands)
        metric_commands.append(per_preset_metric_commands)
        decode_commands.append(per_preset_decode_commands)
        
    generated_commands['encode_commands']    = encode_commands
    generated_commands['metric_commands']    = metric_commands
    generated_commands['copy_commands']      = copy_commands
    generated_commands['downscale_commands'] = downscale_commands
    generated_commands['decode_commands']    = decode_commands

    '''Write the important parameters to a log to act as a double checking mechanism'''
    write_parameters(test_name, rc_values, resolutions, downscale_command_template, encoding_command_template, metric_command_template, test_dir)
    
    return generated_commands


def get_downscaled_clip_list(clip_lists, downscale_target_resolutions, downscale_command_template):
    downscaled_clip_map = dict()
    parameter_tracker = dict()
    downscaled_clip_list = list()
    copy_commands = list()
    downscale_commands = list()
    rawvideo = 'rawvideo'
    fps = '30000/1001'
    resolution_id = list(RESOLUTION.keys())[list(RESOLUTION.values()).index(downscale_target_resolutions)]

    for i in range(len(clip_lists)):
        for clip in clip_lists[i]:
            if clip.endswith('yuv') and yuv_library_found:
                width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
            elif clip.endswith('y4m'):
                width, height, framerate, number_of_frames = read_y4m_header(clip)
            else:
                print('[Warning]: Skipping Clip: {}'.format(clip))
                continue

            '''Assign the pixel format variable based on bitdepth'''
            if clip.endswith('yuv'):
                if bitdepth == 10:
                    pixfmt = "yuv420p10le"
                else:
                    pixfmt = "yuv420p"

            '''xilften has special test case where we change the target resolutions depending on the original source resolution'''
            if 'xilften' in resolution_id:
                if height == 2160:
                    downscale_target_resolutions = [(2560, 1440), (1920, 1080), (1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216)]  # 8bit
                else:
                    downscale_target_resolutions = [(2560, 1088), (1920, 816), (1280, 544), (960, 408), (748, 318), (588, 250), (480, 204), (372, 158)]  # 8bit

            '''remove yuv tokens in case of y4m clip'''
            if clip.endswith('y4m'):
                cleaned_downscale_command_template = remove_yuv_tokens(downscale_command_template)
            else:
                cleaned_downscale_command_template = downscale_command_template
            resized_clips_folder = os.path.join(TEST_SETTINGS['stream_dir'], 'resized_clips')
            
            '''Assign target path for source and scaled clip'''
            resized_clip = os.path.join(resized_clips_folder, os.path.split(clip)[-1])

            '''Remove widthxheight occurences from clip name to avoid detection issues'''
            resized_clip = re.sub(r'\d+x\d+', '', resized_clip).replace('__', '_')

            '''Append source resolution to end of clip'''
            ref_clip = resized_clip[:-4] + '_{width}x{height}'.format(**vars()) + resized_clip[-4:]

            '''Check if resolution is in dict'''
            if "{width}x{height}".format(**vars()) not in downscaled_clip_map:
                downscaled_clip_map["{width}x{height}".format(**vars())] = [ref_clip]
            else:
                downscaled_clip_map["{width}x{height}".format(**vars())].append(ref_clip)

            '''Create copy command'''
            copy_command = "cp {clip} {ref_clip}".format(**vars())
            copy_commands.append(copy_command)

            for target_width, target_height in downscale_target_resolutions:
                if int(target_width) >= int(width) and int(target_height) >= int(height):
                    continue

                '''Name of output downscaled clip'''
                scaled_clip_name = resized_clip[:-4] + '_{width}x{height}to{target_width}x{target_height}'.format(**vars()) + resized_clip[-4:]

                if "{target_width}x{target_height}".format(**vars()) not in downscaled_clip_map:
                    downscaled_clip_map["{target_width}x{target_height}".format(**vars())] = [scaled_clip_name]
                else:
                    downscaled_clip_map["{target_width}x{target_height}".format(**vars())].append(scaled_clip_name)

                '''Fill in downscale template'''
                downscale_command = cleaned_downscale_command_template.format(**vars())

                '''Keep track of parameters of downscaled clips'''
                if resized_clip.endswith('yuv'):
                    parameter_tracker[scaled_clip_name] = [target_width, target_height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                    parameter_tracker[ref_clip] = [width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                if resized_clip.endswith('y4m'):
                    parameter_tracker[scaled_clip_name] = [target_width, target_height, framerate, number_of_frames,target_width]
                    parameter_tracker[ref_clip] = [width, height, framerate, number_of_frames,width]

                downscale_commands.append(downscale_command)

    key_list = sorted(downscaled_clip_map.keys(), key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)

    for target_res in key_list:
        downscaled_clip_list.append(downscaled_clip_map[target_res])

    return copy_commands, downscale_commands, downscaled_clip_list, parameter_tracker


def generate_bash_driver_file(encode_file_id, metric_file_id, metric_id,test_name, test_dir):
    encoding_command_template = test_configurations()[test_name][4]
    resolutions = test_configurations()[test_name][1]

    resized_clips_folder = os.path.join(TEST_SETTINGS['stream_dir'], 'resized_clips')
    encoder_executable_found = re.search(r'\s*\.\/(.*?)\s', encoding_command_template)

    if encoder_executable_found:
        encoder_exec = encoder_executable_found.group(1)
    else:
        print("Warning encoder executable not specified in bash script")

    run_all_paral_file_name = 'run-{}-all-paral.sh'.format(encoder_exec)
    run_paral_cpu_file_name = 'run-{}-paral-cpu.sh'.format(encoder_exec)

    run_all_paral_filepath = os.path.join(test_dir, run_all_paral_file_name)
    run_paral_cpu_filepath = os.path.join(test_dir, run_paral_cpu_file_name)

    with open(run_paral_cpu_filepath, 'w') as file:
        file.write("parallel -j {} < {}-m$1.txt".format(TEST_SETTINGS['num_pool'], encode_file_id))

    run_all_paral_script = []
    run_all_paral_script.append("#!/bin/bash")
    run_all_paral_script.append("encoder_type=\"{}\"".format(encoder_exec))
    run_all_paral_script.append("enc_mode_array=({})".format(' '.join(map(str, TEST_SETTINGS['presets']))))
    run_all_paral_script.append("debug_date=" + r"`date " + "\"+%Y-%m-%d_%H%M%S\"`")
    run_all_paral_script.append("debug_filename=" + "\"debug_output_automation_${debug_date}.txt\"")
    run_all_paral_script.append("mkdir {}".format('bitstreams'))
    run_all_paral_script.append("chmod +rx tools")
    run_all_paral_script.append("chmod +rx tools/*")
    run_all_paral_script.append("chmod +rx *.sh")
    run_all_paral_script.append("chmod +x {}".format(encoder_exec))

    if encoder_exec == 'vvencapp':
        run_all_paral_script.append("chmod +x vvdecapp")
    #if 'vmaf_exe' in metric_id:
    run_all_paral_script.append('mkdir /dev/shm/bitstreams')

# if encoder == 'ffmpeg-libaom' and passes==2:
##        run_all_paral_script.append("mkdir 2pass_logs")

    if TEST_SETTINGS['decode_cycles']:
        run_all_paral_script.append("mkdir {}".format('decode_log_bitstreams/'))

    if resolutions:
        run_all_paral_script.append("rm -rf {resized_clips_folder}".format(**vars()))
        run_all_paral_script.append("mkdir {resized_clips_folder}".format(**vars()))
        run_all_paral_script.append("parallel -j {} < run_copy_reference.txt".format(TEST_SETTINGS['ffmpeg_job_count']))
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j {} < run_downscale.txt) &> time_downscale.log &".format(TEST_SETTINGS['ffmpeg_job_count']))
        run_all_paral_script.append("wait && echo \'Downscaling Finished\'")

    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    run_all_paral_script.append("\tif [ \"$encoder_type\" == \"aomenc\" ] || [ \"$encoder_type\" == \"vpxenc\" ]; then")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) > time_enc_$i.log 2>&1 &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\telse")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) &> time_enc_$i.log &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\tfi\n")
    run_all_paral_script.append("\twait $b && echo \'Encoding Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")

    if 'vmaf_exe' in metric_id:
        metric_jobs = TEST_SETTINGS['vmaf_job_count']
    elif 'SPIE2021' in metric_id:
        metric_jobs = TEST_SETTINGS['vmaf_job_count']
    else:
        metric_jobs = TEST_SETTINGS['ffmpeg_job_count']

    run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(metric_jobs, metric_file_id))
    run_all_paral_script.append("\twait $b && echo \'Running metric commands for Encode Mode \'$i\' Finished\' >> ${debug_filename}")

    run_all_paral_script.append("done")
    if TEST_SETTINGS['decode_cycles']:
        run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
        run_all_paral_script.append("\techo \'Running M\'$i")
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-ffmpeg-decode-m$i.txt) &> time_decode_$i.log &".format(TEST_SETTINGS['decode_jobs'], encode_file_id))
        run_all_paral_script.append("\twait $b && echo \'Running ffmpeg decode for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
        run_all_paral_script.append("done")

    run_all_paral_script.append("python3 PyCollectResults.py")

    with open(run_all_paral_filepath, 'w') as file:
        for line in run_all_paral_script:
            file.write(line + '\n')

    # chmoding bash files
    # NOTE: os.chmod is finicky -> may not work
    file_stat = os.stat(run_all_paral_filepath)
    os.chmod(run_all_paral_filepath, file_stat.st_mode | stat.S_IEXEC)

    file_stat = os.stat(run_paral_cpu_filepath)
    os.chmod(run_paral_cpu_filepath, file_stat.st_mode | stat.S_IEXEC)

    return run_all_paral_filepath


def remove_yuv_tokens(command_template):
    command_tokens = re.findall('\\s*\\S*\\s*{.*?}', command_template)

    for command_token in command_tokens:
        command_param = command_token.split('{')[-1].strip('}')

        if command_param in YUV_PARAMS:
            command_template = command_template.replace(command_token, '')

    return command_template


def write_parameters(test_name, rc_values, resolutions, downscale_command_template, encoding_command_template, metric_command_template, test_dir):
    file_name = '{}-parameters-{}.txt'.format(test_name, datetime.now().strftime("%Y-%m-%d_H%H-M%M-S%S"))
    file_path = os.path.join(test_dir, file_name)
    
    with open(file_path, 'w') as file:
        file.write('test_name: {test_name}\n'.format(**vars()))
        file.write('rc_values: {rc_values}\n'.format(**vars()))
        file.write('resolutions: {resolutions}\n'.format(**vars()))
        file.write('downscale_command_template: {downscale_command_template}\n'.format(**vars()))
        file.write('encoding_command_template: {encoding_command_template}\n'.format(**vars()))
        file.write('metric_command_template: {metric_command_template}\n'.format(**vars()))
        file.write('insert_special_parameters: {}\n'.format(TEST_SETTINGS['insert_special_parameters']))


def write_commands_to_files(generated_commands,test_name, test_dir):  # Good
    '''Get metric command dict name for file naming purposes'''
    metric_command_template= test_configurations()[test_name][3]
    metric_id = list(METRIC_COMMAND.keys())[list(METRIC_COMMAND.values()).index(metric_command_template)]

    encode_commands = generated_commands['encode_commands']
    metric_commands =generated_commands['metric_commands']
    copy_commands =generated_commands['copy_commands']
    downscale_commands =generated_commands['downscale_commands']
    decode_commands = generated_commands['decode_commands']

    if not os.path.isdir(test_dir):
        os.mkdir(test_dir)
        
    '''Define command file names according to their configuration name'''
    encode_file_id = 'run-{}'.format(test_name)
    metric_file_id = 'run-{}'.format(metric_id)
    '''Write Encode and metric Commands'''
    for preset, encode_commands, metric_commands in zip(TEST_SETTINGS['presets'], encode_commands, metric_commands):
        encode_filename = '{}-m{}.txt'.format(encode_file_id, preset)
        metric_filename = '{}-m{}.txt'.format(metric_file_id, preset)
        encode_filepath = os.path.join(test_dir,encode_filename)
        metric_filepath = os.path.join(test_dir,metric_filename)
        
        with open(encode_filepath, 'w') as encode_file,\
             open(metric_filepath, 'w') as metric_file:
                 
            for command in encode_commands:
                encode_file.write('{}\n'.format(command))
            for command in metric_commands:
                metric_file.write('{}\n'.format(command))
                
    '''Write Decode commands'''
    if any(decode_commands):
        for preset, decode_commands in zip(TEST_SETTINGS['presets'], decode_commands):
            decode_filename = '{}-ffmpeg-decode-m{}.txt'.format(encode_file_id, preset)
            decode_filepath = os.path.join(test_dir,decode_filename)
    
            with open(decode_filepath, 'w') as decode_file:
                for command in decode_commands:
                    decode_file.write('{}\n'.format(command))

    if copy_commands and downscale_commands:
        copy_filename = 'run_copy_reference.txt'
        copy_filepath = os.path.join(test_dir,copy_filename)

        downscale_filename = 'run_downscale.txt'
        downscale_filepath = os.path.join(test_dir,downscale_filename)
                
        '''Write Scaling commands'''
        with open(copy_filepath, 'w') as copy_file,\
             open(downscale_filepath, 'w') as downscale_file:
                 
            for command in copy_commands:
                copy_file.write('{}\n'.format(command))
                
            for command in downscale_commands:
                downscale_file.write('{}\n'.format(command))

    return encode_file_id, metric_file_id, metric_id


def insert_new_parameters(token):
    sub_string_first_half = token.rpartition('-i')[0]
    sub_string_second_half = token.rpartition('-i')[2]

    for param in TEST_SETTINGS['insert_special_parameters']:
        sub_string_first_half += param + " "

    sample_command = sub_string_first_half + "-i" + sub_string_second_half
    return sample_command


def generate_decode_commands(input_bin):  # Good
    decode_command_template = '(/usr/bin/time --verbose tools/ffmpeg -threads {decode_threads} -i {input_bin}.bin  -f null - ) > {output_decode_log}.log 2>&1'''
    decode_path = 'decode_log_bitstreams'
    decode_threads = TEST_SETTINGS['decode_threads']

    stream_name = os.path.split(input_bin)[-1]
    output_decode_log = os.path.join(decode_path, stream_name)
    decode_command = decode_command_template.format(**vars())

    return decode_command


'''Two functions to extract clip info from its y4m header'''
def read_y4m_header_helper(readByte, buffer):
    if sys.version_info[0] == 3:
        if (readByte == b'\n' or readByte == b' '):
            clip_parameter = buffer
            buffer = b""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer
    else:
        if (readByte == '\n' or readByte == ' '):
            clip_parameter = buffer
            buffer = ""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer


def read_y4m_header(clip):
    if sys.version_info[0] == 3:
        header_delimiters = {b"W": 'width', b"H": 'height', b"F": 'frame_ratio', b"I": 'interlacing', b"A": 'pixel_aspect_ratio', b"C": 'bitdepth'}
    else:
        header_delimiters = {"W": 'width', "H": 'height', "F": 'frame_ratio', "I": 'interlacing', "A": 'pixel_aspect_ratio', "C": 'bitdepth'}

    y4m_params = {'width': -1,
                  'height': -1,
                  'frame_ratio': -1,
                  'framerate': -1,
                  'number_of_frames': 1,
                  'bitdepth': -1
                  }

    with open(clip, "rb") as f:
        f.seek(10)

        if sys.version_info[0] == 3:
            buffer = b""
        else:
            buffer = ""

        while True:
            readByte = f.read(1)
            if (readByte in header_delimiters.keys()):
                y4m_key = readByte
                while True:
                    readByte = f.read(1)

                    '''Use helper function to interpret byte'''
                    y4m_params[header_delimiters[y4m_key]], buffer = read_y4m_header_helper(readByte, buffer)

                    if y4m_params[header_delimiters[y4m_key]] != -1:
                        break

            if sys.version_info[0] == 3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break

        if sys.version_info[0] == 3:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(b":")
            if b'10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'
        else:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(":")
            if '10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'

        y4m_params['framerate'] = float(frame_ratio_pieces[0]) / float(frame_ratio_pieces[1])

        while f.tell() < os.path.getsize(clip):
            readByte = f.read(1)
            if binascii.hexlify(readByte) == b'0a':
                f.seek(frame_length, 1)
                buff = binascii.hexlify(f.read(5))
                if buff == b'4652414d45':
                    y4m_params['number_of_frames'] += 1

    return int(y4m_params['width']), int(y4m_params['height']), y4m_params['framerate'], y4m_params['number_of_frames']


'''functions to get clip info'''
def get_fps(clipdir, clip):
    clip = os.path.split(clip)[-1]
    if(".yuv" in clip and yuv_library_found):
        seq_table_index = get_seq_table_loc(seq_list, clip)
        if seq_table_index < 0:
            return 0
        fps = float(seq_list[seq_table_index]["fps_num"]) / seq_list[seq_table_index]["fps_denom"]
        return fps
    elif(".y4m" in clip):
        _, _, framerate, number_of_frames = read_y4m_header(os.path.join(clipdir, clip))
        return framerate
    else:
        return 0


def get_seq_table_loc(seq_table, clip_name):
    for i in range(len(seq_table)):
        if seq_table[i]["name"] == clip_name[:-4]:
            return i
    print('{} not found in yuvb library. Exiting...'.format(clip_name))
    sys.exit()


def get_YUV_PARAMS(clip):
    clip_name = os.path.split(clip)[-1]
    seq_table_index = get_seq_table_loc(seq_list, clip_name)

    bitdepth = seq_list[seq_table_index]['bitdepth']
    ten_bit_format = seq_list[seq_table_index]['unpacked']
    width = seq_list[seq_table_index]['width']
    height = seq_list[seq_table_index]['height']
    width_x_height = '%sx%s' % (width, height)
    fps_num = int(seq_list[seq_table_index]['fps_num'])
    fps_denom = int(seq_list[seq_table_index]['fps_denom'])

    if (bitdepth == 8):
        number_of_frames = (int)(os.path.getsize(clip) / (width * height + (width * height / 2)))
        pixel_format = 'yuv420p'
        vvenc_pixel_format = 'yuv420'
    elif (bitdepth == 10):
        pixel_format = 'yuv420p10le'
        vvenc_pixel_format = 'yuv42010'  # Not sure what the real one is
        if ten_bit_format == 2:
            number_of_frames = (int)(((float)(os.path.getsize(clip)) / (width * height + (width * height / 2))) / 1.25)
        else:
            number_of_frames = (int)(((os.path.getsize(clip)) / (width * height + (width * height / 2))) / 2)

    return width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, '%s/%s' % (fps_num, fps_denom), float(float(fps_num) / float(fps_denom)), vvenc_pixel_format, pixel_format


def sort_clip_list_by_complexity(input_folder):
    files = glob.glob('%s/*' % input_folder)

    '''For the case where the key being sorted are identical, perform sub-sorts base on the preceding sorts.'''
    files.sort(key=lambda f: get_fps(input_folder, f), reverse=True)
    files.sort(key=lambda f: f.lower())
    files.sort(key=lambda f: (os.stat(os.path.join(input_folder, f)).st_size), reverse=True)
    Y4M_HEADER_SIZE = 80

    '''Group sorted clips into nested lists based on filesize'''
    clip_lists = []
    clip_list = []
    size_0 = os.stat(os.path.join(input_folder, files[0])).st_size
    for file_ in files:
        if (file_.endswith('yuv') or file_.endswith('y4m')):
            if file_.endswith('yuv') and os.stat(os.path.join(input_folder, file_)).st_size == size_0:
                clip_list.append(file_)
            elif file_.endswith('y4m') and size_0 - Y4M_HEADER_SIZE <= os.stat(os.path.join(input_folder, file_)).st_size <= size_0 + Y4M_HEADER_SIZE:
                clip_list.append(file_)
            else:
                clip_lists.append(clip_list)
                clip_list = []
                size_0 = os.stat(os.path.join(input_folder, file_)).st_size
                clip_list.append(file_)
    clip_lists.append(clip_list)

    return clip_lists


'''Functions to execute test'''
def execute_command(cmd, work_dir):
    """Executes a shell command in a subprocess, waiting until it has completed.
    :param cmd: Command to execute.
    :param work_dir: Working directory path.
    """
    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, error) = pipe.communicate()
    print(out)
    if error:
        print(error)
    pipe.wait()


def execute_test(bash_exe,test_dir):
    #test_dir = os.path.join(os.getcwd(), '{}_{}'.format(test_name, EXECUTION_SETTINGS['commit']))

    run = "{}".format(bash_exe)
    disown = 'disown -h -a'
    copy_tools = 'cp -r tools {}'.format(test_dir)
    copy_collect = 'cp -r PyCollectResults.py {}'.format(test_dir)

    execute_command(copy_tools, os.getcwd())
    execute_command(copy_collect, os.getcwd())
    
    execute_command(run, test_dir)
    execute_command(disown, os.getcwd())




if __name__ == '__main__':
    '''Import the YUV Library to assign parameters for YUV clips which have no embedded meta data'''
    try:
        from yuv_library import getyuvlist
        seq_list = getyuvlist()
        yuv_library_found = 1
    except ImportError:
        print("WARNING yuv_library not found, only generating commands for y4m files.")
        seq_list = []
        yuv_library_found = 0

    '''Python 2 input() doesnt accept strings, replace with raw_input for python 2 case'''
    try:
        input = raw_input
    except NameError:
        pass

    '''Initialize Parameter settings'''
    YUV_PARAMS = ['width', 'height', 'ref_width', 'mod_width', 'ref_height', 'mod_height', 'bitdepth', 'fps_num', 'fps_denom', 'fps', 'vvenc_pixfmt', 'rawvideo', 'pixfmt', 'vmaf_pixfmt', 'frames', 'fps_decimal']

    RESOLUTION = {
        'xilften': [(2560, 1440), (1920, 1080), (1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216), (2560, 1088), (1920, 816), (1280, 544), (960, 408), (748, 318), (588, 250), (480, 204), (372, 158)],
        'xilften_test': [(372, 158)],
        'SPIE2020_8bit': [(1280, 720), (960, 540), (640, 360), (480, 270)],
        'SPIE2020_10bit': [(1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216)],
        'SPIE2021_8bit': [(1280, 720), (960, 540), (768, 432), (640, 360), (512, 288), (384, 216), (256, 144)],
        'fast_testing_resolutions': [(384, 216), (256, 144)],
        'ffmpeg_svt' : [(720,1280),(576,1024),(432,768),(288,512)],

        'SPIE2021_sub_360p': [(640, 360), (512, 288), (384, 216), (256, 144)],

    }

    RC_VALUES = {
        'SPIE2020_x264_x265': [14, 18, 22, 27, 32, 37, 42, 47, 51],
        'SPIE2020_svt_aom': [20, 26, 32, 37, 43, 48, 55, 59, 63],

        'SPIE2021_x264_x265': [19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 41],
        'SPIE2021_svt_aom': [23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63],

        'xilften_crf': [16, 20, 24, 28, 32, 36, 39, 43, 47, 51, 55, 59, 63],
        'generic_tbr': [5000, 4000, 3000, 2000],
        'fast_testing_qps': [50, 51],
        'ffmpeg_svt' : [36, 40, 44, 48, 52, 56],
        
        'svt_mr_testing' : [20,32,43,55,63],
        'webrtc': [75, 150, 300, 600],
        'webrtc_ultra_LD': [300, 600, 1200, 2400],

        'vvenc_5qp' : [22, 27, 32, 42, 51],
        '5qp_preset_tuning' : [32,40,48,55,63],
        '11qp_preset_tuning' : [23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63],
        'avm' : [110, 141, 172, 203, 234],
        'avm_11qp' : [110, 122, 134, 146, 158,170,182,194,206,218,230],

        'vvenc_11qp' :  [19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 41],  
          
    }

    DOWNSCALE_COMMAND = {
        'SPIE2021_scaling': "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags lanczos+accurate_rnd+full_chroma_int -sws_dither none -param0 5  -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {scaled_clip_name}",
        'SPIE2020_scaling': "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags lanczos+accurate_rnd+print_info -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {scaled_clip_name}",
    }

    METRIC_COMMAND = {

        'SPIE2020_ffmpeg_psnr_ssim': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s   {width}x{height} -r 25  -i {clip} -lavfi "ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr" -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_vmaf': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s   {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf|version=vmaf_v0.6.1neg\\:name=vmaf_neg:feature=name=psnr\\:reduced_hbd_peak=true\\:enable_apsnr=true\\:min_sse=0.5|name=float_ssim\\:enable_db=true\\:clip_db=true:log_path={output_filename}.xml:log_fmt=xml' -threads 1 -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_psnr_ssim_vmaf': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip}  -lavfi 'ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr;[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf|version=vmaf_v0.6.1neg\\:name=vmaf_neg:feature=name=psnr\\:reduced_hbd_peak=true\\:enable_apsnr=true\\:min_sse=0.5|name=float_ssim\\:enable_db=true\\:clip_db=true:log_path={output_filename}.vmaf:log_fmt=xml' -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+print_info [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; [scaled1][1:v]ssim=stats_file={output_filename}.ssim;[scaled2][1:v]psnr=stats_file={output_filename}.psnr;[scaled3][1:v]libvmaf=model_path=tools/model/vmaf_v0.6.1.pkl:log_path={output_filename}.vmaf'  -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_rescale_psnr_ssim': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+print_info [scaled][ref];[scaled] split=2 [scaled1][scaled2]; [scaled1][1:v]ssim=stats_file={output_filename}.ssim;[scaled2][1:v]psnr=stats_file={output_filename}.psnr'  -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',

        'SPIE2020_ffmpeg_psnr_ssim_vmaf_neg': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip}  -lavfi 'ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr;[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1neg\\:feature=name=vmaf_neg:log_path={output_filename}.vmaf:log_fmt=xml' -f null - ) > {output_filename}.log 2>&1''',

        'SPIE2021_ffmpeg_vmaf_aom_ctc': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null -) > {output_filename}.log 2>&1''',
        'SPIE2021_ffmpeg_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2021_ffmpeg_vmaf_exe_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref]" -map "[ref]" -f null - -map "[scaled]" -strict -1 -pix_fmt {pixfmt} {temp_clip} && tools/vmaf --reference {ref_clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --aom_ctc v1.0 && rm {temp_clip})> {output_filename}.log 2>&1''',

        'SPIE2021_ffmpeg_rescale_vvenc' : r'''(./vvdecapp -b {output_filename}.bin -o {temp_clip} -t 1 && tools/ffmpeg -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null - && rm {temp_clip}) > {output_filename}.log 2>&1''',

        'SPIE2021_vvenc_ffmpeg_vmaf_exe': r'''(./vvdecapp -b {output_filename}.bin -o {temp_clip} -t 1 && tools/vmaf --reference {clip} --distorted {temp_clip} -w {width} -h {height} -p 420{vmaf_pixfmt} --aom_ctc v1.0 -b {bitdepth} -o {output_filename}.xml && rm {temp_clip}) > {output_filename}.log 2>&1 ''',

        'SPIE2021_ffmpeg_vmaf' : r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s   {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf|version=vmaf_v0.6.1neg\\:name=vmaf_neg:feature=name=psnr\\:reduced_hbd_peak=true\\:enable_apsnr=true\\:min_sse=0.5|name=float_ssim\\:enable_db=true\\:clip_db=true:log_path={output_filename}.xml:log_fmt=xml' -threads 1 -f null - ) > {output_filename}.log 2>&1''',

        'xilften_ffmpeg_vmaf_exe_rescale': r'''(tools/ffmpeg -y -r 25 -i {output_filename}.bin -s:v {ref_width}x{ref_height}  -pix_fmt {pixfmt} -f {rawvideo} -r 25 -i {ref_clip}  -lavfi "scale2ref=flags=lanczos+accurate_rnd+print_info:threads=1 [scaled][ref];[scaled] split=2 [scaled1][scaled2]; [scaled1][1:v]psnr=stats_file={output_filename}.psnr" -map "[ref]" -f null - -map "[scaled2]" -strict -1 -pix_fmt {pixfmt} -f {rawvideo} {temp_clip} && tools/vmaf --reference {ref_clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --nflx_ctc v1.0 --threads 19 && rm {temp_clip}) > {output_filename}.log 2>&1''',

        'webrtc_psnr_ssim': r'''tools/ffmpeg -r 25  -i {output_filename}.bin -r 25 -i {clip} -lavfi "ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr" -f null - > {output_filename}.log 2>&1''',

        'SPIE2021_vtm_ffmpeg' : r'''(tools/DecoderAppStatic -b {output_filename}.bin -o {temp_clip} && tools/ffmpeg -y -nostdin  -r 25 -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null -  && rm {temp_clip}) > {output_filename}.log 2>&1''',
        'SPIE2021_vvenc_ffmpeg' : r'''(tools/vvdecapp -b {output_filename}.bin -o {temp_clip} && tools/ffmpeg -y -nostdin  -r 25 -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null -  && rm {temp_clip}) > {output_filename}.log 2>&1''',

        'SPIE2021_avm_ffmpeg' : r'''(tools/aomdec --codec=av1 --summary -o {temp_clip} {output_filename}.bin && tools/ffmpeg -y -nostdin  -r 25 -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {clip} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null -  && rm {temp_clip}) > {output_filename}.log 2>&1''',
        'SPIE2021_vtm_rescale_ffmpeg' : r'''(tools/DecoderAppStatic -b {output_filename}.bin -o {temp_clip} && tools/ffmpeg -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null - && rm {temp_clip}) > {output_filename}.log 2>&1''',
        'SPIE2021_avm_ffmpeg_rescale' : r'''(tools/aomdec --codec=av1 --summary -o {temp_clip} {output_filename}.bin && tools/ffmpeg -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r 25  -i {temp_clip} -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null -  && rm {temp_clip}) > {output_filename}.log 2>&1''',

    }

    ENCODE_COMMAND = {
        # '''SPIE2020'''
        'SPIE2020_svt_CRF_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp -enc-mode {preset} -q {rc_value} -intra-period {intraperiod} -enable-tpl-la 1 -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'SPIE2020_aom_CRF_2p':     '''(/usr/bin/time --verbose   ./aomenc  --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --passes=2 --verbose  --lag-in-frames=25 --auto-alt-ref=1 --end-usage=q  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',
        'SPIE2020_x264_CRF_1p':    '''(/usr/bin/time --verbose   ./x264  --preset {preset} --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --threads 1  --tune psnr  --stats {output_filename}.stat   -o {output_filename}.bin  {clip})  > {output_filename}.txt 2>&1 ''',
        'SPIE2020_x265_CRF_1p':    '''(/usr/bin/time --verbose   ./x265  --preset {preset} --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth} --frame-threads 1 --no-wpp  --tune  psnr  --stats {output_filename}.stat  {clip}  -o {output_filename}.bin)  > {output_filename}.txt 2>&1 ''',
        'SPIE2020_vp9_CRF_2p':     '''(/usr/bin/time --verbose   ./vpxenc  --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 --auto-alt-ref=6 --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',


        # '''SPIE2021'''
        'SPIE2021_svt_CRF_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value} --lp 1 --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1  --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_aom_CRF_2p':     '''(/usr/bin/time --verbose   ./aomenc --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --passes=2 --verbose  --lag-in-frames=35 --auto-alt-ref=1 --end-usage=q  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom}  -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_x265_CRF_1p':    '''(/usr/bin/time --verbose   ./x265  --preset {preset} --crf {rc_value} --keyint {intraperiod}  --min-keyint {intraperiod} --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --tune  psnr  --stats {output_filename}.stat  --pools 1  --no-scenecut   --no-wpp   {clip}  -o {output_filename}.bin)  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_x264_CRF_1p':    '''(/usr/bin/time --verbose   ./x264  --preset {preset}  --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --threads 1  --tune psnr  --stats {output_filename}.stat  --no-scenecut   -o {output_filename}.bin  {clip})  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_vvenc_CRF_1p':   '''(/usr/bin/time --verbose   ./vvencapp  --input {clip} --preset {vvenc_preset}  --qp {rc_value} --intraperiod {intraperiod} --size {width}x{height}  --format  {vvenc_pixfmt}  --internal-bitdepth {bitdepth}  --fps {fps_num}/{fps_denom}  --threads 1  --output {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_vp9_CRF_2p':     '''(/usr/bin/time --verbose   ./vpxenc --ivf --codec=vp9  --tile-columns=0 --arnr-maxframes=7 --arnr-strength=5 --aq-mode=0 --bias-pct=100 \
            --minsection-pct=1 --maxsection-pct=10000 --i420 --min-q=0 --frame-parallel=0 --min-gf-interval=4 --max-gf-interval=16 --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 \
            --auto-alt-ref=6  --threads=1  --profile=0  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --fps={fps_num}/{fps_denom} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --cq-level={rc_value} --cpu-used={preset} -o  {output_filename}.bin    {clip})  > {output_filename}.txt 2>&1''',

        #'''Default commands'''
        'svt_CRF_1lp_1p':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',
        'svt_CRF_1lp_2p':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1 --passes 2 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',
        'svt_CRF_nonlp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'svt_CRF_nonlp_2p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --passes 2 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'svt_VBR_1lp_1p':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} --tbr {rc_value} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 1 --rc 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'svt_VBR_1lp_2p':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} --tbr {rc_value} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 2 --rc 1 --irefresh-type 2  -i  {clip} --stats {output_filename}.stat -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

        'svt_CRF_lp8_1p':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 8 --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',


        'ffmpeg_svt_fast_decode': '''(/usr/bin/time --verbose ./ffmpeg -y  -s:v {width}x{height}  -pix_fmt {pixfmt}  -r {fps} -f {rawvideo}  -i {clip}  -crf {rc_value}  -preset {preset}  -g {intraperiod}  -threads 1  -c:v libsvtav1  -f ivf   -svtav1-params lp=1:fast-decode=1  {output_filename}.bin ) > {output_filename}.txt 2>&1''',
        'ffmpeg_svt_embedded_scaling' : '''(/usr/bin/time --verbose  ./ffmpeg -hide_banner -y -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height} -r {fps_decimal} -i {ref_clip} -an -threads 1 -pix_fmt {pixfmt} -crf {rc_value} -g {intraperiod} -keyint_min {intraperiod} -movflags faststart -vf scale={target_width}:-2:flags=lanczos:param0=5 -preset {preset} -sc_threshold 0 -c:v libsvtav1 -svtav1-params lp=1 -f mp4 {output_filename}.bin) > {output_filename}.txt 2>&1''',

        'svt_webrtc': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset}  --hierarchical-levels 2  --lp 1  --rc 2 --tbr {rc_value}  --keyint  {intraperiod}  --pred-struct 1   -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',
        'svt_webrtc_SC': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset}  --hierarchical-levels 2  --lp 1  --rc 2 --tbr {rc_value}  --keyint  {intraperiod}  --pred-struct 1  --scm 1   -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'aom_webrtc': '''(/usr/bin/time --verbose   ./aomenc  --passes=1 --rt --end-usage=cbr --disable-trellis-quant=1 --enable-order-hint=0 --enable-global-motion=0  --enable-interintra-comp=0 --enable-cdef=1 --enable-warped-motion=0 --enable-dist-wtd-comp=0 --enable-masked-comp=0  --enable-diff-wtd-comp=0 --enable-interinter-wedge=0 --enable-obmc=0 --enable-filter-intra=0 --enable-dual-filter=0  --enable-restoration=0 --enable-qm=0 --enable-ref-frame-mvs=0 --enable-rect-partitions=0 --enable-intra-edge-filter=0  --enable-smooth-interintra=0 --enable-tx64=0 --enable-smooth-intra=0 --enable-paeth-intra=0 --enable-cfl-intra=0  --enable-palette=0 --enable-intrabc=0 --enable-angle-delta=0 --reduced-tx-type-set=0 --use-intra-dct-only=0  --use-inter-dct-only=0 --use-intra-default-tx-only=0 --enable-interintra-wedge=0 --enable-cfl-intra=0 --aq-mode=3  --tile-rows=0 --tile-columns=0 --row-mt=0 --enable-tpl-model=0 --deltaq-mode=0 --mv-cost-upd-freq=3  --coeff-cost-upd-freq=3 --mode-cost-upd-freq=3 --max-reference-frames=3 --psnr --lag-in-frames=0 --undershoot-pct=50  --overshoot-pct=50 --buf-sz=1000 --buf-initial-sz=600 --buf-optimal-sz=600 --max-intra-rate=300 --threads=1  --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --target-bitrate={rc_value} --cpu-used={preset}  -o  {output_filename}.bin    {clip}  )  > {output_filename}.txt 2>&1 ''',
        'aom_webrtc_SC': '''(/usr/bin/time --verbose   ./aomenc  --passes=1 --rt --end-usage=cbr --disable-trellis-quant=1 --enable-order-hint=0 --enable-global-motion=0  --enable-interintra-comp=0 --enable-cdef=1 --enable-warped-motion=0 --enable-dist-wtd-comp=0 --enable-masked-comp=0  --enable-diff-wtd-comp=0 --enable-interinter-wedge=0 --enable-obmc=0 --enable-filter-intra=0 --enable-dual-filter=0  --enable-restoration=0 --enable-qm=0 --enable-ref-frame-mvs=0 --enable-rect-partitions=0 --enable-intra-edge-filter=0  --enable-smooth-interintra=0 --enable-tx64=0 --enable-smooth-intra=0 --enable-paeth-intra=0 --enable-cfl-intra=0  --enable-palette=1 --enable-intrabc=0 --tune-content=screen --enable-angle-delta=0 --reduced-tx-type-set=0 --use-intra-dct-only=0  --use-inter-dct-only=0 --use-intra-default-tx-only=0 --enable-interintra-wedge=0 --enable-cfl-intra=0 --aq-mode=3  --tile-rows=0 --tile-columns=0 --row-mt=0 --enable-tpl-model=0 --deltaq-mode=0 --mv-cost-upd-freq=3  --coeff-cost-upd-freq=3 --mode-cost-upd-freq=3 --max-reference-frames=3 --psnr --lag-in-frames=0 --undershoot-pct=50  --overshoot-pct=50 --buf-sz=1000 --buf-initial-sz=600 --buf-optimal-sz=600 --max-intra-rate=300 --threads=1  --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --target-bitrate={rc_value} --cpu-used={preset}  -o  {output_filename}.bin    {clip}  )  > {output_filename}.txt 2>&1 ''',

        'vtm_CRF_1p' : '''(/usr/bin/time --verbose   ./EncoderAppStatic  --InputFile={clip} --QP={rc_value} -c encoder_randomaccess_vtm.cfg --SourceWidth={width} --SourceHeight={height}  --InputChromaFormat=420  --InputBitDepth={bitdepth}  --FrameRate={fps_decimal} --FramesToBeEncoded={number_of_frames}  --BitstreamFile={output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'vvenc_CRF_1p' : '''(/usr/bin/time --verbose   ./vvencapp  --input {clip} --preset {vvenc_preset}  --qp {rc_value} --intraperiod {intraperiod} --size {width}x{height}  --format  {vvenc_pixfmt} --internal-bitdepth {bitdepth}  --fps {fps_num}/{fps_denom}  --threads 1  --output {output_filename}.bin )  > {output_filename}.txt 2>&1''',

        'svt_CRF_1lp_1p_open_gop' : '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value} --keyint {intraperiod} -w {width} -h {height}  --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 --irefresh-type 1  --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',
        'SPIE2021_vvenc_CRF_1p_closedGOP':   '''(/usr/bin/time --verbose   ./vvencapp  --input {clip} --preset {vvenc_preset}  --qp {rc_value} --intraperiod {intraperiod} --size {width}x{height}  --format  {vvenc_pixfmt}  --internal-bitdepth {bitdepth}  --fps {fps_num}/{fps_denom}  --threads 1 --refreshtype idr --output {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'avm_CRF_1p' : '''(/usr/bin/time --verbose   ./aomenc --verbose --codec=av1 -v --psnr --obu --cpu-used={preset} --frame-parallel=0 --passes=1 --end-usage=q --i420  --use-fixed-qp-offsets=1 --deltaq-mode=0  --enable-tpl-model=0 --fps={fps_num}/{fps_denom}  --input-bit-depth={bitdepth} --bit-depth={bitdepth} -w {width} -h {height} --qp={rc_value} --tile-columns=0 --threads=1  --enable-fwd-kf=0  --enable-keyframe-filtering=0  --min-gf-interval=16 --max-gf-interval=16 --gf-min-pyr-height=4 --gf-max-pyr-height=4 --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --lag-in-frames=19 --auto-alt-ref=1  -o {output_filename}.bin {clip})  > {output_filename}.txt 2>&1''',
        'avm_CRF_6L_1p' : '''(/usr/bin/time --verbose   ./aomenc --verbose --codec=av1 -v --psnr --obu --cpu-used={preset} --frame-parallel=0 --passes=1 --end-usage=q --i420  --use-fixed-qp-offsets=1 --deltaq-mode=0  --enable-tpl-model=0 --fps={fps_num}/{fps_denom}  --input-bit-depth={bitdepth} --bit-depth={bitdepth} -w {width} -h {height} --qp={rc_value} --tile-columns=0 --threads=1  --enable-fwd-kf=0  --enable-keyframe-filtering=0  --min-gf-interval=32 --max-gf-interval=32 --gf-min-pyr-height=5 --gf-max-pyr-height=5 --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --lag-in-frames=35 --auto-alt-ref=1  -o {output_filename}.bin {clip})  > {output_filename}.txt 2>&1''',
        'svt_CRF_1lp_1p_aq0':   '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 --aq-mode 0 --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

    }

    main()
