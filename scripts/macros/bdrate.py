import math

# Python port of the Bjontegaard Metric implementation for Excel
# Original authors:
#    Tim Bruylants, ETRO, Vrije Universiteit Brussel


def bdRateExtend(rateA, distA, rateB, distB, bMode, bRange):
    # type: (list[float], list[float], list[float], list[float], str, bool) -> float

    minPSNRA = float(min(distA))
    maxPSNRA = float(max(distA))
    minPSNRB = float(min(distB))
    maxPSNRB = float(max(distB))

    minMinPSNR = min(minPSNRA, minPSNRB)
    maxMinPSNR = max(minPSNRA, minPSNRB)
    minMaxPSNR = min(maxPSNRA, maxPSNRB)
    maxMaxPSNR = max(maxPSNRA, maxPSNRB)

    minPSNR = minMinPSNR
    maxPSNR = minMaxPSNR

    if bRange:
        if (minPSNRA > maxPSNRB or minPSNRB > maxPSNRA):
            bdRate = 0
        else:
            bdRate = (maxPSNR - maxMinPSNR) / (maxPSNRA - minPSNRA)
            
        if (maxPSNRB < maxPSNRA):
            bdRate = -1 * bdRate

        return bdRate

    if (bMode == "LowAlways"):
        minPSNR = minMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "HighAlways"):
        minPSNR = maxMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "BothAlways"):
        minPSNR = minMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "None" or not (minPSNRA > maxPSNRB or minPSNRB > maxPSNRA)):
        if (minPSNRA > maxPSNRB):
            return 1
        if (minPSNRB > maxPSNRA):
            return -1
        minPSNR = maxMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "Low"):
        minPSNR = minMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "High"):
        minPSNR = maxMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "Both"):
        minPSNR = minMinPSNR
        maxPSNR = maxMaxPSNR

    vA = bdRIntEnh(rateA, distA, minPSNR, maxPSNR)
    vB = bdRIntEnh(rateB, distB, minPSNR, maxPSNR)
    avg = (vB - vA) / (maxPSNR - minPSNR)

    bdRate = pow(10, avg) - 1
    return bdRate

#  Enhanced BD Rate computation method that performs extrapolation instead of
#  Computing BDRate within the "common interval, when and only when there is no PSNR overlap.


def bdRIntEnh(rate, dist, low, high, distMin=0, distMax=1E+99):
    # type: (list[float], list[float], float, float, int, int) -> float
    elements = len(rate)

    log_rate = [0] * (elements + 3)
    log_dist = [0] * (elements + 3)

    rate, dist, log_rate, log_dist, elements = addValues(
        rate, dist, log_rate, log_dist, elements)

    # Remove duplicates and sort data
    log_rate = sorted(set(log_rate))
    log_dist = sorted(set(log_dist))

    log_rate.append(0)
    log_rate.append(0)
    log_dist.append(0)
    log_dist.append(0)

    # If plots do not overlap, extend range
    # Extrapolate towards the minimum if needed
    if (log_dist[1] > low):
        for i in range(1, elements):
            log_rate[elements + 2 - i] = log_rate[elements + 1 - i]
            log_dist[elements + 2 - i] = log_dist[elements + 1 - i]

        elements = elements + 1
        log_dist[1] = low
        log_rate[1] = log_rate[2] + (low - log_dist[2]) * \
            (log_rate[2] - log_rate[3]) / (log_dist[2] - log_dist[3])

    # Extrapolate towards the maximum if needed
    if (log_dist[elements] < high):
        log_dist[elements + 1] = high
        log_rate[elements + 1] = log_rate[elements] + (high - log_dist[elements]) * (
            log_rate[elements] - log_rate[elements - 1]) / (log_dist[elements] - log_dist[elements - 1])
        elements = elements + 1

    result = intCurve(log_dist, log_rate, low, high,
                         elements, distMin, distMax)

    return result


def addValues(rate, dist, log_rate, log_dist, elements):
    # type: (list[float], list[float], list[float], list[float], int) -> tuple[list[float], list[float], list[float], list[float], int]
    i = 1
    for j in range(elements, 0, -1):
        # Add elements only if they are not empty
        if (len(rate) != 0 and len(dist) != 0 and rate[j-1] > 0 and dist[j-1] > 0):
            log_rate[i] = math.log(rate[j-1], 10)
            log_dist[i] = dist[j-1]
            i += 1
    elements = i - 1

    return rate, dist, log_rate, log_dist, elements


def pchipend(h1, h2, del1, del2):
    # type: (float, float, float, float) -> float
    D = ((2 * h1 + h2) * del1 - h1 * del2) / (h1 + h2)
    if (D * del1 < 0):
        D = 0
    elif ((del1 * del2 < 0) and (abs(D) > abs(3 * del1))):
        D = 3 * del1

    return D


def intCurve(xArr, yArr, low, high, elements,  distMin=0, distMax=1E+99):
    # type: (list[float], list[float], float, float, int, int, int) -> float

    H = [0] * (elements + 3)
    delta = [0] * (elements + 3)

    for i in range(1, elements):
        H[i] = xArr[i + 1] - xArr[i]
        delta[i] = (yArr[i + 1] - yArr[i]) / H[i]

    D = [0] * (elements + 3)

    D[1] = pchipend(H[1], H[2], delta[1], delta[2])

    for i in range(2, elements):
        D[i] = (3 * H[i - 1] + 3 * H[i]) / ((2 * H[i] + H[i - 1]) /
                                            delta[i - 1] + (H[i] + 2 * H[i - 1]) / delta[i])

    D[elements] = pchipend(H[elements - 1], H[elements - 2],
                           delta[elements - 1], delta[elements - 2])
    C = [0] * (elements + 3)
    B = [0] * (elements + 3)

    for i in range(1, elements):
        C[i] = (3 * delta[i] - 2 * D[i] - D[i + 1]) / H[i]
        B[i] = (D[i] - 2 * delta[i] + D[i + 1]) / (H[i] * H[i])

    result = 0
    #    Compute rate for the extrapolated region if needed
    s0 = xArr[1]
    s1 = xArr[2]

    for i in range(1, elements):
        s0 = xArr[i]
        s1 = xArr[i + 1]

        #  clip s0 to valid range
        s0 = max(s0, low, distMin)
        s0 = min(s0, high, distMax)

        #  clip s1 to valid range
        s1 = max(s1, low, distMin)
        s1 = min(s1, high, distMax)

        s0 = s0 - xArr[i]
        s1 = s1 - xArr[i]

        if (s1 > s0):
            result = result + (s1 - s0) * yArr[i]
            result = result + (s1 * s1 - s0 * s0) * D[i] / 2
            result = result + (s1 * s1 * s1 - s0 * s0 * s0) * C[i] / 3
            result = result + (s1 * s1 * s1 * s1 - s0 *
                               s0 * s0 * s0) * B[i] / 4

    return result
